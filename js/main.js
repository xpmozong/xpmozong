jQuery(function ($) {
    'use strict';
	
	$(window).scroll(function() {
		if (window.innerWidth > 990) {
			/*定义滚动条距顶端距离*/
			var ww = $(document).scrollTop();
			if (ww > 345) {
				$("#mright").addClass('fixed');
			} else {
				$("#mright").removeClass('fixed');
			}
		}
	})	
});
